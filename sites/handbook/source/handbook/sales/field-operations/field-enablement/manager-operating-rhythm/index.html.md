---
layout: handbook-page-toc
title: "Manager Operating Rhythm"
description: "A consistent management operating rhythm (MOR) fosters efficient, predictable growth by codifying best practices for how front-line managers in GitLab's field organization lead their teams"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## Overview
A consistent management operating rhythm (MOR) fosters efficient, predictable growth by codifying best practices for how front-line managers in GitLab's field organization lead their teams via Planning & Execution, Talent Management, and Visibility. MORs identify key manager actions and responsibilities with guidance on how to perform, suggested cadence, standard tools and reports, and more. Click on the links below to learn more.
- [Enterprise Area Sales Manager (ASM) MOR](/handbook/sales/field-operations/field-enablement/manager-operating-rhythm/ent-asm-mor/)
- Mid Market Area Sales Manager (ASM) MOR
- SMB Area Sales Manager (ASM) MOR
- Channel Sales/Account Manager (CSM/CAM) Leader MOR
- Solution Architect (SA) Manager MOR
- Technical Account Manager (TAM) MOR
