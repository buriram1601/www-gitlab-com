---
layout: handbook-page-toc
title: "Enterprise Area Sales Manager Manager Operating Rhythm"
description: "Successful management includes onboarding, reviewing Command Plans, opportunity coaching, strategic coaching, career development and performance management"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview
- [ENT ASM MOR 1-pager](https://docs.google.com/spreadsheets/d/1s_QMqt4a3UKKZkIdf82N5840_ygPE4z7mpgpniSCsNc/edit?usp=sharing)

## Planning and Execution

### Prioritization
- _Cadence: Quarterly_
- Account Ranking
- Expand Account P1 Team Review (CSP)
- New Land Accounts (FMM/SDR)
- Starter to Premium/Ultimate focus accounts

### Account Reviews
- _Cadence: Monthly / Ad-hoc_
- P1/2 Account Planning

### Partner Reviews
- _Cadence: Bi-weekly_
- Review key partner alignment (with Alliance/Channel) via Partner Plan & Review Sheet

### Opportunity Reviews
- _Cadence: Weekly_
- [Opportunity Consults](/handbook/sales/command-of-the-message/opportunity-consults/)
- [Enterprise Dashboards](/handbook/sales/field-operations/sales-operations/#sales-operations-sponsored-dashboards-and-maintenance)
    - Pipeline Dashboard
    - Current Quarter (CQ) Sales Dashboard
    - Pipeline Health Check
- SAL Heatmap
- Clari Dashboards
    - [Pipeline Health Dashboard](https://app.clari.com/dashboard?dashboardId=5b96eb8f9258b900014297ad)
    - [Rep/Manager 1x1 Dashboard](https://app.clari.com/dashboard?dashboardId=605a14753f9d77039390026f)

### Forecasting 
- [Sales Forecast Dashboard](https://app.clari.com/dashboard?dashboardId=606f0ac5ac043e61f9ebeb8d) (Clari)

#### In-Quarter Forecasting
- _Cadence: Weekly_
- Review Commit & Best Case
- Review Top Opportunities
- Submit Forecast

#### Out-Quarter Forecasting
- _Cadence: Monthly_
- Q+1 & Q+2 Forecast (Team Call)
    - Stage 3+ Opps (summary of CP)
        - Compelling event
        - Risk
        - Contracts and Paper process
    - Pipeline Status vs OKR
    - Priority actions
        - New Logo LAND Target Account Actions/Issues
        - Starter to Premium/Ultimate opps/quotes
        - Existing Customer Ultimate pursuits - actions/help needed"

### Sales Calls
- _Cadence: Ad-hoc / Daily_
- Plan for the Call
    - [Develop a Pre-Call Plan](/handbook/sales/playbook/discovery/#develop-a-pre-call-plan)
- Participate in the Call
- Debrief the Call

## Talent Management

### Recruiting
- _Cadence: Ongoing_
- Build Talent Network (Always Be Recruiting)
    - Networking Tools
- Identify Talent
    - Success Profile
- Interview Skills
    - Interview Guide
    - "Why You" Summary

### Onboarding
- _Cadence: Ongoing_
- Review Onboarding Checklist
    - General GitLab onboarding issue
    - Field onboarding training (Sales Quick Start)
- Set Performance Expectations
- Monitor 30/60/90 Day Performance

### Performance Management
- _Cadence: Semi-annual_
- Conduct mid-year and annual assessment

### Developing and Retaining
- _Cadence: Ongoing_
- Skill/Will Model
- Coaching Model
- Development Plan

### Team Assessment
- _Cadence: Semi-annual_
- [Field Functional Competencies](/handbook/sales/training/field-functional-competencies/)
- [Performance / Growth Matrix](/handbook/people-group/talent-assessment/#the-performancegrowth-matrix)

## Visibility

### GTM Planning
- _Cadence: Annually_
- Recommend Territory Assignments
- Recommend Quotas
- Identify Resource Requirements

### Quarterly Business Reviews
- _Cadence: Quarterly_
- Distributed each quarter; each slide has the reports linked
- Stored [here](https://drive.google.com/drive/u/0/folders/1QAWFYxT2-NzWpOUodepbl0O6zfsMxJGL)
