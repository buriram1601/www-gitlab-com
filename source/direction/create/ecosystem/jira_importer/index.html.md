---
layout: markdown_page
title: "Category Direction - Jira Importer"
description: This is the category direction for Jira Importer in GitLab; which is part of the Plan stage's Project Management group.
canonical_path: "/direction/create/ecosystem/jira_importer/"
---

Last reviewed: 2020-07-15

- TOC
{:toc}

## 🚥 Jira Importer

|          |                                |
| -------- | ------------------------------ |
| **Stage** | [Create](/direction/dev/#create-1) | 
| **Maturity** | [Planned](/direction/minimal/)  |
| **Last reviewed** | 2021-01-16 |

[Issue List](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AJira%20Importer)
- [Overall Ecosystem Direction](/direction/create/ecosystem/)

_This direction is a work in progress, and [everyone can contribute](#contributing):_

* Please comment, thumbs-up (or down!), and contribute to the linked issues and 
  epics on this category page. Sharing your feedback directly on GitLab.com is 
  the best way to contribute to our vision.
* Alternately, please share feedback directly via [email](mailto:pdeuley@gitlab.com) or 
  [Twitter](https://twitter.com/gitlab). If you're a GitLab user and have direct
  knowledge of your need from a particular integration, we'd love to hear from you.

### Overview

Jira is one of the most common project and portfolio managmenet tools with roughly 34% market share. Many GitLab customers that purchase GitLab for source code management
or CI/CD functionality continue to use Jira due to:

- The lack of an easy migration path from Jira to GitLab.
- [Missing capabilities](https://gitlab.com/groups/gitlab-org/-/epics/2586) within GitLab that are necessary for teams to plan and manage their work effectively.

We are focusing on providing a great Jira importing experience in lock step with improving GitLab's planning capabilities. It is our goal to make it effortless for our customers to start realizing the increased value and efficiencies that are unlocked when project and product management is colocated within the same application as the rest of the DevOps lifecycle.

### Where we are Headed

#### What's Next & Why

These are the epics and issues that we will be working on over the next few releases:

- Extend the importer to support additional data mapping as outlined in the [follow-up iterations](https://gitlab.com/groups/gitlab-org/-/epics/2738#follow-up-iterations-aka-what-the-mvc-doesnt-do). We are currently working towards supporting [mapping users from Jira to GitLab](https://gitlab.com/gitlab-org/gitlab/-/issues/210580) so imported comments and issue activity are correctly attributed to the author. We are also focusing on [building a better parser](https://gitlab.com/gitlab-org/gitlab/-/issues/212295) to handle converting Jira issue descriptions to GitLab Flavored Markdown.
  - **Outcome:** Jira issues can be imported to GitLab without losing contextual information such as comments and their original authors, issue dependencies and relationships, and epics. We provide verification that 100% of the targeted issues were successfully imported into GitLab.
  - **Progress:** [Basic user mapping](https://gitlab.com/groups/gitlab-org/-/epics/3095) is nearing completion and will likely ship with 13.2 or 13.3. We also shipped the [first iteration of a parser](https://gitlab.com/groups/gitlab-org/-/epics/2926) to convert Atlassian ADF to GitLab flavored markdown as part of 13.2.

#### What is Not Planned Right Now

- Importing complex workflows, triggers, and automations from Jira to GitLab. As GitLab's capabilities mature, we will revisit whether or not this would provide additional value for teams looking to move from Jira to GitLab.
- Importing Confluence wikis to GitLab wikis. While we intend on supporting this at some point in the future, we are currently focused on creating a Jira Software importer that helps teams transition their issue management to GitLab.

#### Maturity Plan

This category is currently at the 😊**Minimal** maturity level. We are tracking our progress against becoming 😁**Viable** via [this epic](https://gitlab.com/groups/gitlab-org/-/epics/2738).

### User success metrics

- 100% of targeted issues are imported successfully from Jira into GitLab.
- Count of total issues imported.
- Count of projects that imported issues from Jira to GitLab.
- Count of instances using the Jira issue importer.

### Top Customer Success/Sales issue(s)

- None at this time.

### Top user issue(s)

- [Jira Importer](https://gitlab.com/groups/gitlab-org/-/epics/2738) (👍 47, +0, +3)

### Top Strategy Item(s)

This is a non-marketing category which was created as a specific strategy to help drive our 1 year plan of [importing from Jira without losting required data](https://about.gitlab.com/direction/dev/#plan). While the importer itself is an important component of this, we also need to maintain [focus on adding capabilities](https://gitlab.com/groups/gitlab-org/-/epics/2586) to GitLab that enable us to win successfully against competitors like Jira.

We will likely be working towards [merging the work we've done on the importer with the Jira integration](https://gitlab.com/groups/gitlab-org/-/epics/3681) so users have the option to complete a 1 time import and maintain an ongoing 2 way data syncronization between Jira Issues and GitLab Issues.
