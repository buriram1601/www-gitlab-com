$(function() {
  $('.experiment-control .plans').slick({
    appendDots: '.experiment-control .pricing-nav-mobile',
    dots: false,
    centerMode: false,
    infinite: false,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: false,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          centerMode: true,
          dots: true,
          initialSlide: 1,
          slidesToShow: 1,
          slidesToScroll: 1,
          variableWidth: true
        }
      }
    ]
  });

  if ($('.experiment-test').length) {
    $('.experiment-test .plans').slick({
      appendDots: '.experiment-test .pricing-nav-mobile',
      dots: false,
      centerMode: false,
      infinite: false,
      speed: 300,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: false,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            centerMode: true,
            dots: true,
            initialSlide: 1,
            slidesToShow: 1,
            slidesToScroll: 1,
            variableWidth: true
          }
        }
      ]
    });
  };
});

var initializeBenefitsViewMore = function()
{
  var benefitsViewToggle = document.querySelectorAll('.benefits-view-more, .benefits-view-less');
  for(loopcount=0; loopcount < benefitsViewToggle.length; loopcount++)
  {

    benefitsViewToggle[loopcount].addEventListener('click', function()
    {
      // expand the stuff we want to be visible after click
      var expandThis = document.querySelectorAll('.benefits-view');
      for(loopcount2=0; loopcount2 < expandThis.length; loopcount2++)
      {
        expandThis[loopcount2].classList.toggle("hidden");
      };
      // hide the stuff we want hidden after click
      var benefitsViewMore = document.querySelectorAll('.benefits-view-more');
      for(loopcount3=0; loopcount3 < benefitsViewMore.length; loopcount3++)
      {
        benefitsViewMore[loopcount3].classList.toggle("hidden");
      };
    });
  };
};

window.initializeBenefitsViewMore = initializeBenefitsViewMore;
